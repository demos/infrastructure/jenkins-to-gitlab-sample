# Jenkins to GitLab CI sample
At the root of this repo is a base `.gitlab-ci.yml` which defines all of the steps needed to get started. There is also a `Jenkinsfile` that is the final state pipeline. The goal is to convert the `Jenkinsfile` into equivalent steps in GitLab CI.

To do this, go through the directories named step1-step5 sequentially. In each is a `Jenkinsfile` and a `.gitlab-ci.yml`. The `Jenkinsfile` will have functionality to port over between 2 comments as described below. The `.gitlab-ci.yml` will contain a starting point for the step and `# TODO` comments that describe what need to be added and link to the relevant documentation. Use this information to modify the `.gitlab-ci.yml at the base of this repository to complete each step.


Comments in `Jenkinsfile` that denote the new functionality to create:
```groovy
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
New functionality to port over
//==========================================================================
```

Comment in `.gitlab-ci.yml` to describe where to add new functionality:
```yaml
# TODO: ... (link)
```


At the root, there also exists a `Dockerfile` which we will use to demonstrate building and pushing container images to GitLab's registry and a `sources.md` which contains links to sources referenced for our `Jenkinsfile`.